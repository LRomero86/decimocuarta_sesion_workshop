//const env = require("./enviroment.json");
//const puerto = env.development.PORT;

const express = require("express");
const server = express();
server.use(express.urlencoded({extended:true}));
server.use(express.json())

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

var indiceUser;
var resultado;

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'swagger clase 14',
        version: '1.0.0'
      }
    },
    apis: ['./server.js'],
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);

server.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));//localhost:3000/api-docs


//Middleware de validación de login
function validarUsuario(req, res, next) {

    if(!req.body.user || !req.body.pass){
        res.status(404).send("SE REQUIEREN TODOS LO CAMPOS");
    }else{
        usuarios.map((item) => {

            if (item.user == req.body.user && item.pass == req.body.pass) {
                console.log('ES VALIDO!');
            }else{
                console.log('NO ES VALIDO: ESTAMOS EN MIDDLEWARE');
            }
        });
    }
    next();
}

var usuarios = [
    {
        user:"administrador",
        otrakey:"",
        pass:"1234",
        admin:true,
    }
   /** {
        id:Number,
        user:String,
        nom_ape:String,
        email:String,
        tel:Number,
        direc:String,
        pass:String,
        admin:Boolean
    }*/
]

var productos = [
    {
        id:Number,
        stock:Number,
        nom:String,
    }
]

var pedidos = [
    {
        id:Number,
        productos:[
            {
                id:Number,
                nom:String,
            }
        ],
        cantidad:Number,
        id_usuario:Number,
    }
]

/**
 * @swagger
 * /usuarios:
 *  post:
 *    description: Creacion de usuario
 *    parameters: 
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: true
 *      description : id del usuario
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre de usuario
 *    - name: nom_ape
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre y apellido del usuarios
 *    - name: email
 *      type: string
 *      in: formData
 *      required: true
 *      description: correo electronico del usuario
 *    - name: telefono
 *      type: integer
 *      in: formData
 *      required: true
 *      description: telefono del usuario
 *    - name: direccion
 *      type: string
 *      in: formData
 *      required: true
 *      description: direccion del usuario
 *    - name: pass
 *      type: string
 *      in: formData
 *      required: true
 *      description: contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

/*
server.post("/usuarios", validarUsuario, function(req,res) {
    
    const datos = {        
        user:req.body.user,
        nom_ape:req.body.nom_ape,
        email:req.body.email,
        tel:req.body.telefono,
        direc:req.body.direccion,
        pass:req.body.pass,
        admin:false
    }

    usuarios.push(datos)

    res.status(201).send(usuarios);
    /*
        if(req.body.id !=="" || req.body.user !=="" || req.body.nom_ape !=="" || req.body.email !=="" || req.body.telefono !=="" || req.body.direccion !=="" || req.body.pass !=="" ){
            const datos = {
                id:parseInt(req.body.id),
                user:req.body.user,
                nom_ape:req.body.nom_ape,
                email:req.body.email,
                tel:req.body.telefono,
                direc:req.body.direccion,
                pass:req.body.pass,
                admin:false
            }
            usuarios.push(datos);
            res.status(201).send("SE CREO EXITOSAMENTE EL NUEVO USUARIO")
        }
    }
    
});
*/

/**
 * @swagger
 * /usuario/login:
 *  post:
 *    description: Creacion de usuario
 *    parameters: 
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre de usuario 
 *    - name: pass
 *      type: string
 *      in: formData
 *      required: true
 *      description: contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */


server.post('/usuario/login', validarUsuario, function(req, res) {
    //aquí tengo que ingresar mis datos de login
    login = {
        user:req.body.user,
        pass:req.body.pass,
    }
    res.status(201).send('valido?');

});

/**
 * @swagger
 * /login:
 *  post:
 *    description: Verificar existencia del usuario
 *    parameters: 
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description : usuario
 *    - name: pass
 *      type: string
 *      in: formData
 *      required: true
 *      description : contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

/*
server.post("/login",(req,res)=>{
    let resultado;
    if(!req.body.user || !req.body.pass){
        res.status(404).send("SE REQUIEREN TODOS LO CAMPOS")
        console.log('usuario: ', user, ', password: ', pass);
    }else{
        if(req.body.user !== "" || req.body.pass !== ""){ // busca en el array USUARIO
            usuarios.map((item,index)=>{
                if(item.user === req.body.user && item.pass === req.body.pass){
                    resultado = item;
                    console.log('resultado: ', resultado);
                    index = index;
                    res.status(201).send(index)
                }
            });
        }
        if(resultado == undefined){
            res.status(201).send('lalala');
            //res.status(404).send("El usuario no se encontro")
        }
    }
});

*/

//middlewares para validar login de usuario y middleware para validar si está logueado


server.get('/productos', (req, res) => {
    if (resultado == true) {
        res.status(201).send(productos);
    }else{
        res.status(201).send('Usted no está logueado');
    }
});

server.listen(3000, function() {
    console.log('corriendo puerto 3000 en appPedidos');
});



/**
 * 
 */
